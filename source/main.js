function openMenu() {
  var options = document.getElementsByClassName("menu-option");
  var value   = options["0"].style.getPropertyValue('display');

  if (value == "" || value == "none"){
    for (option in options) {
      options[option].style.display = "block";
    }
  }else if(value == "block"){
    for (option in options) {
      options[option].style.display = "none";
    }
  }
}
