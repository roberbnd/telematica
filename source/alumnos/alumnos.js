function irAlumnos () {
  fetch('./source/alumnos/alumnos.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irMoodle () {
  window.location.href = 'http://telematicanet.ucol.mx/moodle/'
}

function irActividades () {
  fetch('./source/alumnos/actividades.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('alumnos').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irProcesos () {
  fetch('./source/alumnos/procesos/main.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irServicio () {
  fetch('./source/alumnos/procesos/servicio.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-procesos').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irServicioConstitucional () {
  fetch('./source/alumnos/procesos/constitucional.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-procesos').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irEstancia () {
  fetch('./source/alumnos/procesos/estancia.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-procesos').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irServicio () {
  fetch('./source/alumnos/procesos/servicio.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-procesos').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irHorarios () {
  fetch('./source/alumnos/horario.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irCalendario () {
  window.location.href = 'http://www.ucol.mx/content/portal/265_CALENDARIO%20ESCOLAR%202017-2018.pdf';
}

function irGuia () {
  window.location.href = 'http://portal.ucol.mx/content/micrositios/227/file/GUIA_DEL_ESTUDIANTE_AGOSTO_2017.pdf';
}

function irBiblioteca () {
  window.location.href = 'http://bvirtual.ucol.mx/';
}

function irTesis () {
  window.location.href = 'http://telematicanet.ucol.mx/tesis/'
}
