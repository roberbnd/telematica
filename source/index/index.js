function changeLangEs () {
  document.getElementById('en').classList.remove('lang-selected');
  document.getElementById('es').classList.add('lang-selected');
  document.getElementById('video-english').style.display = 'none';
  document.getElementById('video-spanish').style.display = 'block';
}

function changeLangEn () {
  document.getElementById('es').classList.remove('lang-selected');
  document.getElementById('en').classList.add('lang-selected');
  document.getElementById('video-spanish').style.display = 'none';
  document.getElementById('video-english').style.display = 'block';
}

function oferTelematica () {
  fetch('./source/oferta/telematica.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}

function oferSoftware () {
  fetch('./source/oferta/software.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}
