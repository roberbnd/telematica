function irFacultad () {
  fetch('./source/facultad/facultad.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irFacultadContacto () {
  fetch('./source/facultad/facultad-contacto.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-facultad').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irFacultadPersonal () {
  fetch('./source/facultad/facultad-personal.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-facultad').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irFacultadReglamento () {
  fetch('./source/facultad/facultad-reglamento.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-facultad').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irFacultadInfraestructura () {
  fetch('./source/facultad/facultad-infra.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-facultad').innerHTML = data
  }).catch(err => {
    return err
  });
}
