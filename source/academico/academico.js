function irAcademico () {
  fetch('./source/academico/academico-main.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('header').style.minHeight = '2em';
    document.getElementById('content').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irAcademias () {
  fetch('./source/academico/academico-academias.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-academico').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irCuerpos () {
  fetch('./source/academico/academico-cuerpo.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-academico').innerHTML = data
  }).catch(err => {
    return err
  });
}

function irPrograma () {
  fetch('./source/academico/academico-programa.html', {
    method: 'get'
  }).then(response => {
    return response.text();
  }).then(data => {
    document.getElementById('content-academico').innerHTML = data
  }).catch(err => {
    return err
  });
}
